import asyncio
import uuid

from fastapi import APIRouter, WebSocket, WebSocketDisconnect
from fastapi.encoders import jsonable_encoder
from loguru import logger

from app.handlers.scanner import scanner_manager
from app.handlers.websocket import ws
from app.schemas.api import InComingData, OutGoingData

router = APIRouter()


@router.websocket("/address")
async def websocket_endpoint(websocket: WebSocket):
    # Generate a UUID for the client
    client_id = uuid.uuid4().hex

    # Add the client ID to the WebSocket scope
    websocket.scope["client_id"] = client_id

    await ws.connect(websocket)
    try:
        while True:
            msg = await websocket.receive_json()
            incoming_data = InComingData(**msg)
            for address in incoming_data.addresses:
                # Check if a lock exists for this address, if not create one
                if address not in ws.locks:
                    ws.locks[address] = asyncio.Lock()

                try:
                    # Acquire the lock for this address
                    async with ws.locks[address]:
                        # Process the address and send the response
                        async for outgoing_data in scanner_manager.to_ws(
                            client_id=client_id,
                            chain_id=incoming_data.chain_id,
                            address=address,
                            block_start=incoming_data.block_start,
                            block_end=incoming_data.block_end,
                        ):
                            await ws.send_json(
                                client=websocket, data=jsonable_encoder(outgoing_data)
                            )
                except Exception as err:
                    error_data = jsonable_encoder(err)
                    error_data["client_id"] = client_id
                    error_data["uid"] = address
                    await ws.send_json(client=websocket, data=error_data)
            break
        # await ws.disconnect(websocket)
    except Exception as err:
        await ws.disconnect(websocket)
