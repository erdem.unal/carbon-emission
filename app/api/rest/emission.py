from eth_typing import HexAddress
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlalchemy.ext.asyncio import AsyncSession

from app.config.database import get_db_session
from app.models.emission import CarbonEmission
from app.schemas.api import AggregatedData, ChainId

router = APIRouter()


@router.get("/emissions", response_model=list[AggregatedData])
async def get_address_data(
    block_start: int = 0,
    block_end: int = 999999999,
    addresses: list[HexAddress] = Query(...),
    chain_id: ChainId = Query(...),
    conn: AsyncSession = Depends(get_db_session),
):
    try:
        response = []
        for address in addresses:
            result = await CarbonEmission.get_aggregated_result(
                address, block_start, block_end, chain_id, session=conn
            )
            if result is not None:
                response.append(AggregatedData(**result))
        return response
    except Exception as err:
        raise HTTPException(status_code=404, detail=str(err))
