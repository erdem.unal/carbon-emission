from datetime import datetime
from uuid import UUID, uuid4

from sqlalchemy.orm import Mapped, declarative_mixin, declared_attr, mapped_column

from app.utils.helpers import set_timestamp


@declarative_mixin
class DBMixin:
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower() + "_table"

    # __mapper_args__ = {"always_refresh": True}

    id: Mapped[UUID] = mapped_column(insert_default=uuid4, primary_key=True)
    created_at: Mapped[datetime] = mapped_column(insert_default=set_timestamp)
    updated_at: Mapped[datetime] = mapped_column(
        insert_default=set_timestamp, onupdate=set_timestamp
    )
