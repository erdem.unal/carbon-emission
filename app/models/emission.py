from loguru import logger
from sqlalchemy import Enum, Float, Integer, String, UniqueConstraint, func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.types import BigInteger

from app.config.database import Base
from app.models.base import DBMixin
from app.schemas.api import ChainId


class CarbonEmission(DBMixin, Base):
    __table_args__ = (
        UniqueConstraint("chain_id", "address", "block_start", name="_unique_block"),
    )

    chain_id: Mapped[int] = mapped_column(Integer(), nullable=False)
    address: Mapped[str] = mapped_column(String(), nullable=False)
    block_start: Mapped[int] = mapped_column(Integer(), nullable=False)
    block_end: Mapped[int] = mapped_column(Integer(), nullable=False)
    timestamp_start: Mapped[int] = mapped_column(Integer(), nullable=False)
    timestamp_end: Mapped[int] = mapped_column(Integer(), nullable=False)
    tx_fee: Mapped[int] = mapped_column(BigInteger(), nullable=False)
    tx_count: Mapped[int] = mapped_column(Integer(), nullable=False)
    gas_used: Mapped[int] = mapped_column(BigInteger(), nullable=False)
    co2: Mapped[float] = mapped_column(Float(), nullable=False)

    @classmethod
    async def get_aggregated_result(
        cls,
        address: str,
        block_start: int,
        block_end: int,
        chain_id: ChainId,
        session: AsyncSession,
    ):
        result = await session.execute(
            select(
                func.min(cls.block_start),
                func.max(cls.block_end),
                func.min(cls.timestamp_start),
                func.max(cls.timestamp_end),
                func.sum(cls.gas_used),
                func.sum(cls.tx_count),
                func.sum(cls.tx_fee),
                func.sum(cls.co2),
            ).where(
                cls.address == address,
                cls.block_start >= block_start,
                cls.block_end <= block_end,
                cls.chain_id == chain_id.value,
            )
        )

        row = result.one()
        if row[0] is None:
            return None
        return {
            "chain_id": chain_id,
            "address": address,
            "block_start": row[0],
            "block_end": row[1],
            "timestamp_start": row[2],
            "timestamp_end": row[3],
            "gas_used": int(row[4]),
            "tx_count": row[5],
            "tx_fee": int(row[6]),
            "co2": row[7],
        }

    @classmethod
    async def get_latest_block_number(
        cls, address: str, chain_id: ChainId, session: AsyncSession
    ):
        stmt = select(func.max(cls.block_end)).where(
            cls.address == address, cls.chain_id == chain_id.value
        )
        result = (await session.execute(stmt)).scalar_one_or_none()
        return result + 1 if result is not None else 0
