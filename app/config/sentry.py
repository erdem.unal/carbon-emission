import os

import sentry_sdk
from sentry_sdk.integrations.asyncio import AsyncioIntegration
from sentry_sdk.integrations.cloud_resource_context import (
    CloudResourceContextIntegration,
)

# from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration


def setup_sentry() -> None:
    sentry_sdk.init(
        dsn=os.environ.get("SENTRY_DSN"),
        traces_sample_rate=1.0,
        environment=os.environ.get("APP_ENV"),
        release=os.environ.get("APP_VERSION"),
        integrations=[
            AsyncioIntegration(),
            # SqlalchemyIntegration(),
            # CloudResourceContextIntegration(),
        ],
    )
