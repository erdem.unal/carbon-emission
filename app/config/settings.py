import os

from loguru import logger
from pydantic import BaseSettings, HttpUrl

from app.schemas.api import ChainId


class Settings(BaseSettings):
    APP_NAME: str = "Carbon Emission API"
    APP_ENV: str = "development"
    APP_VERSION: str = "0.1.0"
    LOG_PATH: str = "app.log"
    RATE_LIMIT: int = 4
    OFFSET: int = 10000
    DTFORMAT: str = "%Y-%m-%d"
    ROOT_PATH: str = "/carbon-emission/v1"
    SENTRY_DSN: str

    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_HOST: str
    POSTGRES_PORT: int
    POSTGRES_DB: str

    ETH__API_KEY: str
    ETH__URL: HttpUrl = "https://api.etherscan.io/api"
    BNB__API_KEY: str
    BNB__URL: HttpUrl = "https://api.bscscan.com/api"
    MATIC__API_KEY: str
    MATIC__URL: HttpUrl = "https://api.polygonscan.com/api"
    CELO__API_KET: str = ""
    CELO__URL: HttpUrl = "https://api.celoscan.com/api"
    CCRI__API_KEY: str
    CCRI__URL: HttpUrl = "https://v2.api.ccri.tech"
    CCRI__MERGE_DATE: str = "2022-09-15"

    class Config:
        env_nested_delimiter = "__"
        env_file_encoding = "utf-8"
        case_insensitive = True

        @classmethod
        def get_env_file(cls):
            if os.environ.get("APP_ENV") == "production":
                return None
            else:
                return ".env"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @classmethod
    def provider(cls, chain_id: ChainId) -> tuple[HttpUrl, str]:
        api_key: str = getattr(cls(), f"{chain_id.name}__API_KEY")
        api_url: HttpUrl = getattr(cls(), f"{chain_id.name}__URL")
        return (api_url, api_key)


settings = Settings()

# logger.add(sys.stderr, level="DEBUG")
logger.add(settings.LOG_PATH, rotation="500 MB", level="INFO")
