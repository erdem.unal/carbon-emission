import json

from aiocache import Cache


class CacheManager:

    """
    This module provides a CacheManager class that abstracts access to an AIocache instance. The CacheManager is used to store and retrieve dictionaries that map inner keys (strings) to float values. The dictionaries are stored in the cache under a key that corresponds to the outer key provided by the caller.
    - CacheManager: A class that provides methods to store and retrieve dictionaries that map inner keys to float values in the cache.

    An example cache: {"2022-01-01": '{"eth": 0.2, "bnb": 0.01}',"2022-01-02": '{"eth": 0.3}}
    """

    def __init__(self, ttl: int = 86400):
        """
        To instantiate a CacheManager with a default time-to-live (TTL) of 86400 seconds (1 day), create an instance like this:
        async_cache = CacheManager(ttl=86400)
        """
        self.ttl = ttl
        self.cache = Cache(Cache.MEMORY, ttl=ttl)

    async def set_many(
        self, inner_key: str, values: list[dict[str, float]], ttl: int = None
    ) -> bool:
        """
        Stores multiple dictionaries in the cache.

        :param inner_key: The key for the inner dictionary.
        :param values: A list of dictionaries containing float values to be stored in the cache.
        :param ttl: The time-to-live for the cached values in seconds. If not specified, the default TTL of the cache manager is used.
        :return: True if all dictionaries were successfully stored in the cache, raises an exception otherwise.
        """
        pairs = []
        for value in values:
            for key, val in value.items():
                if not await self.cache.exists(key):
                    pairs.append((key, json.dumps({inner_key: val})))
                else:
                    existing_val = await self.cache.get(key)
                    metadata = json.loads(existing_val)
                    metadata[inner_key] = val
                    pairs.append((key, json.dumps(metadata)))

        await self.cache.multi_set(pairs, ttl=self.ttl if ttl is None else ttl)
        return True

    async def get(self, key: str, inner_key: str) -> float:
        """
        Retrieves a single value from the cache.

        :param key: The key of the outer dictionary containing the inner dictionary to retrieve the value from.
        :param inner_key: The key of the inner dictionary containing the value to retrieve.
        :return: The float value associated with the given key and inner_key, or None if either key is not in the cache
                or the inner_key is not in the dictionary for the given key.
        """
        cache_value = await self.cache.get(key)
        if cache_value is None:
            return None

        value_dict = json.loads(cache_value)
        if inner_key not in value_dict:
            return None

        return value_dict[inner_key]

    async def get_keys_by_inner_key(self, inner_key: str) -> list[str]:
        """
        Returns a list of keys for all outer dictionaries that contain the specified inner key.

        :param inner_key: The key of the inner dictionary to search for.
        :return: A list of keys for all outer dictionaries containing the specified inner key.
        """
        result = []
        for key, value in self.cache._cache.items():
            metadata = json.loads(value)
            if inner_key in metadata:
                result.append(key)
        return result

    async def get_many(self, keys: list[str], inner_key: str) -> list[float]:
        """
        <<<TODO>>> possible bug for the case that order of items in keys must match with result

        Retrieve a list of float values associated with the given inner key from multiple cache keys.

        :param keys: A list of cache keys to retrieve values from.
        :param inner_key: The inner key to retrieve float values from.
        :return: A list of float values, where the i-th element corresponds to the i-th key in `keys`.
            If a key's value is not found or the inner key is not present, the corresponding value will be `None`.
        """
        values = []
        for key in keys:
            cache_value = await self.cache.get(key)
            if cache_value is not None:
                value_dict = json.loads(cache_value)
                if inner_key in value_dict:
                    values.append(value_dict[inner_key])
                else:
                    values.append(None)
            else:
                values.append(None)
        return values


async_cache = CacheManager(ttl=86400)
