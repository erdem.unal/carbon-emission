from fastapi import WebSocket


class WebsocketManager:
    def __init__(self):
        self.clients = set()
        self.locks = {}

    async def send_json(self, client, data):
        try:
            await client.send_json(data)
        except Exception as e:
            raise Exception(e)

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.clients.add(websocket)

    async def disconnect(self, websocket: WebSocket):
        self.clients.remove(websocket)

    def __str__(self):
        return f"WebsocketManager(clients={len(self.clients)})"


ws = WebsocketManager()
