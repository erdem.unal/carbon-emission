import asyncio

import aiohttp
from eth_typing.evm import HexAddress
from loguru import logger
from sqlalchemy.ext.asyncio import AsyncSession

from app.config.database import engine
from app.config.settings import settings
from app.handlers.emission import TransactionManager
from app.models.emission import CarbonEmission
from app.schemas.api import ChainId, OutGoingData
from app.schemas.scanner import TransactionRequest, TransactionResponse
from app.utils.exceptions import APICallException


async def prepare_transaction_request(
    chain_id: ChainId,
    address: HexAddress,
    block_start: int,
    block_end: int,
) -> tuple[str, TransactionRequest]:
    api_url, api_key = settings.provider(chain_id)

    async with AsyncSession(engine) as session:
        latest_block_number = await CarbonEmission.get_latest_block_number(
            address=address, chain_id=chain_id, session=session
        )
    tx_request = TransactionRequest(
        address=address,
        startblock=latest_block_number
        if latest_block_number is not None
        else block_start,
        endblock=block_end if block_end > 0 else 99999999,
        apikey=api_key,
    )

    return api_url, tx_request


async def prepare_websocket_response(
    client_id: str,
    chain_id: ChainId,
    address: HexAddress,
    tx_response=TransactionResponse,
) -> OutGoingData:
    df = TransactionManager(address=address, chain_id=chain_id, data=tx_response)
    blockwise_aggregated = await df.aggregated_blocks()

    async with AsyncSession(engine) as session:
        emission = CarbonEmission(**blockwise_aggregated.dict())
        session.add(emission)
        await session.commit()
        await session.refresh(emission)
    return OutGoingData(client_id=client_id, uid=address, result=blockwise_aggregated)


class ScannerManager:
    def __init__(self):
        self.semaphores = {}  # dictionary to store semaphores for each endpoint
        # self.session = aiohttp.ClientSession()

    async def get_transactions_by_address(
        self, url: str, params: TransactionRequest
    ) -> TransactionResponse:
        """
        Returns the list of transactions performed by an address for specified block range.
        See the doc: https://docs.etherscan.io/api-endpoints/accounts

        :param url: uri required to make api call
        :param params: serialized api params
        :return: serialized api response
        """

        try:
            async with aiohttp.ClientSession() as session:
                async with self.semaphores[url]:
                    async with session.get(url, params=params.dict()) as response:
                        if response.status != 200:
                            error_message = f"Failed to fetch data from {url}. Status code: {response.status}"
                            raise Exception(error_message)
                        result = await response.json()
                        return TransactionResponse.parse_obj(result)
        except aiohttp.ClientError as e:
            # Handle any other client errors (e.g. connection timeout, DNS resolution errors)
            raise Exception(f"Failed to fetch data from {url}. Exception: {e}")

    async def to_ws(
        self,
        client_id: str,
        chain_id: ChainId,
        address: HexAddress,
        block_start: int,
        block_end: int,
    ):
        url, tx_request = await prepare_transaction_request(
            chain_id=chain_id,
            address=address,
            block_start=block_start,
            block_end=block_end,
        )

        if url not in self.semaphores:
            self.semaphores[url] = asyncio.Semaphore(settings.RATE_LIMIT)

        block_start = tx_request.startblock

        while True:
            tx_request.startblock = block_start

            if tx_request.startblock >= tx_request.endblock:
                raise APICallException(message="Hit the latest block!")

            try:
                tx_response = await self.get_transactions_by_address(
                    url=url, params=tx_request
                )
            except Exception as err:
                raise APICallException(message="Failed to make request to api!")

            if tx_response.status == 0:
                raise APICallException(message="No transaction found!")

            outgoing_data = await prepare_websocket_response(
                client_id=client_id,
                chain_id=chain_id,
                address=tx_request.address,
                tx_response=tx_response,
            )
            outgoing_data.uid = address
            yield outgoing_data

            if outgoing_data.result and outgoing_data.result.tx_count < settings.OFFSET:
                raise APICallException(message="No transaction found!")

            if outgoing_data.result:
                block_start = outgoing_data.result.block_end + 1


scanner_manager = ScannerManager()
