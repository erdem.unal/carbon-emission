import asyncio
from bisect import bisect_left
from datetime import datetime

import aiohttp
from loguru import logger

from app.config.settings import settings
from app.handlers.cache import async_cache
from app.schemas import ccri
from app.schemas.api import ChainId


class CCRIManager:

    """
    See: https://docs.api.carbon-ratings.com/


    Currently, for each date, one api call be done.
    API accepts up to 77 dates within request body.

    https://v2.api.ccri.tech/currencies/eth2/emissions/gasConsumption/tx-based
    CCRI default values:
    inputUnit -> 'ether'
    outputUnit -> 'kg'

    https://v2.api.ccri.tech/currencies/eth2/emissions/gasConsumption/tx-based
    CCRI default values:
    inputUnit -> 'gas'
    outputUnit -> 'g'

    ERROR - NOT COVERED:
    {'entries': [], 'metaData': {'sum': 0.0, 'mean': 0.0, 'inputUnit': 'gas',
    'outputUnit': 't', 'description': 'Allocated the emissions for Ethereum to
    gasConsumption with a tx-based allocation strategy, gas as input and t as output.',
    'warnings': ["The requested date '2020-02-27' is not coverd by our API for currency
    eth2, ignored.", 'The entries list is empty, therefore the sum and mean is defined
    as zero.']}}

    """

    def __init__(self):
        self.url = "https://v2.api.ccri.tech/currencies/{ticker}/emissions/{inputDataType}/{allocationStrategy}"
        self.limit = (
            70  # maximum request object (number of dates in response is max 77)
        )
        self.merge_date = datetime.strptime(
            settings.CCRI__MERGE_DATE, settings.DTFORMAT
        )

    async def fetch(
        self,
        session: aiohttp.ClientSession,
        url: str,
        payload: ccri.EmissionRequest,
        semaphore: asyncio.Semaphore,
    ) -> ccri.EmissionResponse:
        async with semaphore:
            async with aiohttp.ClientSession(
                headers={"key": settings.CCRI__API_KEY}
            ) as session:
                async with session.post(url, json=payload.dict()) as resp:
                    result = await resp.json()
                    return ccri.EmissionResponse(**result)

    async def concurrent_fetch(
        self, chain_id: ChainId, url=str, payloads=list[ccri.EmissionRequest]
    ) -> None:
        limit = asyncio.Semaphore(value=5)  # TODO check here
        async with aiohttp.ClientSession() as session:
            tasks = []
            for payload in payloads:
                task = asyncio.ensure_future(self.fetch(session, url, payload, limit))
                tasks.append(task)
            responses = await asyncio.gather(*tasks)
            for response in responses:
                await asyncio.sleep(0.5)
                await async_cache.set_many(
                    inner_key=chain_id.name,
                    values=[{i.date: i.outputValue} for i in response.entries],
                )

    async def set_tx_based_pow_request_url(self, chain_id: ChainId) -> str:
        return self.url.format(
            ticker=chain_id.name.lower(),
            inputDataType=ccri.InputDataType.TXFEE,
            allocationStrategy=ccri.AllocationStrategy.TXBASED,
        )

    async def set_tx_based_pow_request_body(
        self, dates=list[str]
    ) -> ccri.EmissionRequest:
        """
        "inputUnit": "gwei",
        "outputUnit": "kg",
        """
        return ccri.EmissionRequest(
            entries=[ccri.EntriesInRequest(date=d) for d in dates],
            parameters=ccri.EmissionParams(
                inputUnit=ccri.InputUnit.GWEI, outputUnit=ccri.OutputUnit.KG
            ),
        )

    async def set_tx_based_pos_request_url(self, chain_id: ChainId) -> str:
        return self.url.format(
            ticker=ccri.Ticker.ETH2.value
            if chain_id.value == 1
            else chain_id.name.lower(),
            inputDataType=ccri.InputDataType.GASCONSUMPTION,
            allocationStrategy=ccri.AllocationStrategy.TXBASED,
        )

    async def set_tx_based_pos_request_body(
        self, dates=list[str]
    ) -> ccri.EmissionRequest:
        return ccri.EmissionRequest(
            entries=[ccri.EntriesInRequest(date=d) for d in dates],
            parameters=ccri.EmissionParams(outputUnit=ccri.OutputUnit.KG),
        )

    async def get_tx_based_pow(self, chain_id: ChainId, dates=list[str]) -> None:
        cached_dates = await async_cache.get_keys_by_inner_key(inner_key=chain_id.name)
        target_dates = list(set(dates) - set(cached_dates))
        if target_dates:
            chunks = [
                target_dates[i : i + self.limit]
                for i in range(0, len(target_dates), self.limit)
            ]
            url = await self.set_tx_based_pow_request_url(chain_id=chain_id)
            payloads = [await self.set_tx_based_pow_request_body(p) for p in chunks]
            await self.concurrent_fetch(chain_id, url, payloads)

    async def get_tx_based_pos(self, chain_id: ChainId, dates=list[str]) -> None:
        cached_dates = await async_cache.get_keys_by_inner_key(inner_key=chain_id.name)
        target_dates = list(set(dates) - set(cached_dates))
        if target_dates:
            chunks = [
                target_dates[i : i + self.limit]
                for i in range(0, len(target_dates), self.limit)
            ]
            url = await self.set_tx_based_pos_request_url(chain_id=chain_id)
            payloads = [await self.set_tx_based_pos_request_body(c) for c in chunks]
            await self.concurrent_fetch(chain_id, url, payloads)

    async def set_coefficients(self, chain_id: ChainId, dates=list[str]) -> None:
        if chain_id.value == 1:
            # Split list of dates into two based on separator a.k.a merge_date
            separator_index = bisect_left(dates, settings.CCRI__MERGE_DATE)

            # ALL DATES BEFORE CCRI MERGE DATE YYYY-MM-DD
            before_dates = dates[:separator_index]  # dates before merge
            if before_dates:
                await self.get_tx_based_pow(chain_id=chain_id, dates=before_dates)

            # ALL DATES AFTER CCRI MERGE DATE YYYY-MM-DD
            after_dates = dates[separator_index:]  # dates after merge
            if after_dates:
                await self.get_tx_based_pos(chain_id=chain_id, dates=after_dates)
        else:
            await self.get_tx_based_pos(chain_id=chain_id, dates=dates)

    async def get_coefficients(
        self, timestamps: list[str], chain_id: ChainId
    ) -> list[float]:
        dates = [
            datetime.utcfromtimestamp(int(ts)).strftime("%Y-%m-%d") for ts in timestamps
        ]
        coefficient_values = await async_cache.get_many(
            keys=dates, inner_key=chain_id.name
        )
        return coefficient_values


ccri_manager = CCRIManager()
