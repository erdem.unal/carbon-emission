from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from loguru import logger
from fastapi.openapi.utils import get_openapi

from app.api.api import router
from app.config.database import close_db, init_db
from app.config.sentry import setup_sentry
from app.config.settings import settings

# Get the version from the pyproject.toml file using the poetry command

setup_sentry()

app = FastAPI(
    # root_path=settings.ROOT_PATH,
    title=settings.APP_NAME,
    version=settings.APP_VERSION,
    contact={
        "name": "Erdem Unal",
        "url": "https://calculator.mentholprotocol.com",
        "email": "erdem@mentholprotocol.com",
    },
    description="A python-based backend service for carbon-emission calculation",
)


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup_event():
    logger.info("Starting up...")
    await init_db()


@app.on_event("shutdown")
async def shutdown_event():
    logger.info("Shutting down...")
    await close_db()


app.include_router(router)


@app.get("/health-check")
async def health():
    return {"status": "ok"}
