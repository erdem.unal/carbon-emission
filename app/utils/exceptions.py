class APICallException(Exception):
    """Raised when an error occurs during an API call"""

    def __init__(self, message):
        self.status = "0"
        self.message = message
        self.result = {}
        super().__init__(self.message)


class DatabaseException(Exception):
    """Raised when an error occurs during an API call"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)


class RateLimitError(Exception):
    """Raised when the rate limit for an API has been exceeded"""

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)
