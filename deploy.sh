#!/bin/bash

# Script by Erdem Unal erdem@mentholprotocol.com

CURRENT_RELEASE=$(poetry version | awk '{print $NF}')

echo "Which type of release is this? (major, minor, or patch)"
read RELEASE_TYPE

RELEASE_TYPE=$(echo $RELEASE_TYPE | tr '[:upper:]' '[:lower:]')

poetry version $RELEASE_TYPE

read -p "Do you want to build the Docker image? (y/n) " BUILD_IMAGE

if [ "$BUILD_IMAGE" == "y" ]; then
    RELEASE_VERSION=$(poetry version | awk '{print $NF}')
    RELEASE_ENV=production
    IMAGE_TAG="registry.gitlab.com/menthol-protocol/carbon-emission:$RELEASE_VERSION"

    docker buildx build --build-arg APP_VERSION=$RELEASE_VERSION -t $IMAGE_TAG --build-arg APP_ENV=$RELEASE_ENV .

    echo "Docker build complete! Image tag: $IMAGE_TAG"
    read -p "Do you want to push the Docker image? (y/n) " PUSH_IMAGE

    if [ "$PUSH_IMAGE" == "y" ]; then
        docker push $IMAGE_TAG
        echo "Docker push complete! Image tag: $IMAGE_TAG"
    else
        echo "Docker push cancelled."
    fi

else
    poetry version $CURRENT_RELEASE
    echo "Docker build cancelled. Version reverted to $CURRENT_RELEASE."
fi
