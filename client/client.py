import argparse
import asyncio
import json
import time

import websockets


async def send_message(uri, request_data):
    start_time = time.time()
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps(request_data))
        async for message in websocket:
            # handle received message from server
            print(message)
    end_time = time.time()
    execution_time = end_time - start_time
    print(f"Execution time: {execution_time/60} minutes")


async def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description="Concurrent WebSocket client")
    parser.add_argument(
        "-n", type=int, default=10, help="Number of concurrent connections"
    )
    args = parser.parse_args()
    no_of_clients = args.n

    addresses = [
        # "0xfa1B15dF09c2944a91A2F9F10A6133090d4119BD",
        # "0xdAC17F958D2ee523a2206206994597C13D831ec7",
        # "0xae0Ee0A63A2cE6BaeEFFE56e7714FB4EFE48D419",
        # "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
        # "0x00000000000001ad428e4906aE43D8F9852d0dD6",
        # "0x7D1AfA7B718fb893dB30A3aBc0Cfc608AaCfeBB0",
        # "0xEf1c6E67703c7BD7107eed8303Fbe6EC2554BF6B",
        # "0xb6041eae62c4591458af480679c6a497eda6cfcd",
        # "0x8a90CAb2b38dba80c64b7734e58Ee1dB38B8992e",
        # "0x685295860aD11A3Aece64220694d89992169f721"
        # "0x8537b194BFf354c4738E9F3C81d67E3371DaDAf8" #ipDAI
        # "0x9Bd2177027edEE300DC9F1fb88F24DB6e5e1edC6" #ipUSDT
        # "0x7c0e72f431FD69560D951e4C04A4de3657621a88" #ipUSDC
        # "0xf93E0edc76f3147C63F53E7eD245330b96009B26" #ivDAI
        # "0xe966d1cAe4770CEFE9410e9D14D9486fFee19048" #ivUSDT
        # "0xE176F879eE386b4a4bf31B5704b500854952a95C" #ivUSDC
        "0x2F800Db0fdb5223b3C3f354886d907A671414A7F"
    ]

    # Define the message to be sent
    message = {
        "chain_id": 1,
        "addresses": addresses,
        "block_start": 0,
        "block_end": 0,
    }
    # url = "ws://localhost:8004/ws/address"
    url = "wss://api.mentholprotocol.com/carbon-emission/v1/ws/address"

    # Connect to the server using multiple clients
    tasks = []
    for i in range(no_of_clients):
        tasks.append(asyncio.ensure_future(send_message(url, message)))
    await asyncio.gather(*tasks)


if __name__ == "__main__":
    asyncio.run(main())
