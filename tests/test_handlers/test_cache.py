import pytest

from app.handlers.cache import CacheManager


@pytest.fixture
def async_cache():
    cache = CacheManager()
    yield cache


@pytest.mark.asyncio
async def test_set_many(async_cache):
    values = [
        {"2022-01-01": 1.0},
        {"2022-01-02": 2.0},
        {"key3": 3.0},
    ]
    result = await async_cache.set_many("2022-03-05", values)
    assert result is True


@pytest.mark.asyncio
async def test_get(async_cache):
    await async_cache.set_many("2022-03-05", [{"eth": 1.0}])
    result = await async_cache.get("eth", "2022-03-05")
    assert result == 1.0


@pytest.mark.asyncio
async def test_get_invalid_key(async_cache):
    result = await async_cache.get("eth", "2022-03-05")
    assert result is None


@pytest.mark.asyncio
async def test_get_invalid_inner_key(async_cache):
    await async_cache.set_many("2022-03-05", [{"eth": 1.0}])
    result = await async_cache.get("eth", "invalid_inner_key")
    assert result is None


@pytest.mark.asyncio
async def test_get_keys_by_inner_key(async_cache):
    await async_cache.set_many("eth", [{"2022-01-01": 1.0}, {"2022-01-02": 2.0}])
    await async_cache.set_many("bnb", [{"2022-01-01": 1.0}, {"2022-01-03": 3.0}])
    result = await async_cache.get_keys_by_inner_key("eth")
    assert set(result) == set(["2022-01-01", "2022-01-02"])


@pytest.mark.asyncio
async def test_get_many_values(async_cache):
    await async_cache.set_many(
        "eth", [{"2022-05-01": 0.1}, {"2022-06-01": 0.3}, {"2022-06-04": 0.5}]
    )
    await async_cache.set_many(
        "bnb", [{"2022-05-01": 0.2}, {"2022-07-01": 0.4}, {"2022-10-01": 0.6}]
    )

    result = await async_cache.get_many(
        ["2022-05-01", "2022-05-01", "2022-06-01", "2022-06-04"], "eth"
    )
    assert result == [0.1, 0.1, 0.3, 0.5]

    result = await async_cache.get_many(
        ["2022-05-01", "2022-07-01", "2022-10-01"], "bnb"
    )
    assert result == [0.2, 0.4, 0.6]
